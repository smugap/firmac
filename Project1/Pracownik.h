#pragma once
#include "Osoba.h"
#include <iostream>
#include <iomanip>
#include <string>
using namespace std;
class Pracownik :
	public Osoba
{
	string skill;
	friend class Zespol;
	friend class File;
public:
	Pracownik();
	Pracownik(const Pracownik& rhs);
	Pracownik(string imie, string skil);
	~Pracownik() ;
	void setSalary(double Salary) override;
	void show() override;
	void setSkill(string s);
	string wtp();


};

