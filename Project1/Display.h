#pragma once
#include <iostream>
#include <conio.h>
#include <string>
#include <sstream>
#include "File.h"
#include "Kierownik.h"
#include "Osoba.h"
#include "Pracownik.h"
#include "View.h"
#include "Zespol.h"
#include "PracownikController.h"
#include "KierownikController.h"
#include "FinanseController.h"
#include "ZespolController.h"

#include <cctype> //tolower
using namespace std;
class Display
{
	
public:
	void menuZespol();
	void menuZespolShowAll(ZespolController &pra);
	static void menuAddZespol(ZespolController &pra);
	void menuDeleteZespol(ZespolController &pra);
	static void menuZespolEdit(ZespolController &pra);
	void menuPracownik();
	void menuKierownik();
	void menuFinanse();
	static void menuAbout();
	void menuExit(char& opcja);
	void menuKierownikAdd(KierownikController &pra);
	void menuKierownikShow(KierownikController &pra);
	void menuKierownikEdit(KierownikController &pra);
	void menuKierownikDelete(KierownikController &pra);
	void menuPracownikAdd(PracownikController &pra);
	void menuPracownikShow(PracownikController &pra);
	void menuPracownikEdit(PracownikController &pra);
	void menuPracownikDelete(PracownikController &pra);
	void menuFinanseWydatki();
	void menuFinanseAll();
	static void rysuj(string komunikat, string tresc,string opcja="");
	static void wyczyscEkran();
	void rysujMenu();
	Display();
	~Display();
};

