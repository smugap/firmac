#include "Pracownik.h"



Pracownik::Pracownik():Osoba()
{
	m_salary *= 1.5;
	m_stanowisko = "Pracownik";
	skill = "brak";
	m_imie = "Piotr";
}

Pracownik::Pracownik(const Pracownik & rhs):Osoba(rhs)
{
	skill = rhs.skill;
}


Pracownik::Pracownik(string imie,  string skil):Osoba(imie,"Pracownik")
{
	skill = skil;
	m_salary *= 1.5;
}

Pracownik::~Pracownik()
{
	
}

void Pracownik::setSalary(double Salary)
{
	m_salary = Salary;
}

void Pracownik::show()
{
	Osoba::show();
	
	cout << setw(14)  <<"Umiejetnosc: " << setw(15) << skill << " | " << endl;
}

void Pracownik::setSkill(string s)
{
	skill = s;
}
string Pracownik::wtp()
{
	string pomoc = Osoba::wtp();

	pomoc += skill;
	
	return pomoc;
}