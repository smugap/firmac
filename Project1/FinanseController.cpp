#include "FinanseController.h"

void FinanseController::showAll()
{
	KierownikController a;
	PracownikController b;
	ZespolController c;
	cout << "Wszyscy zatrudnieni: " << endl;
	a.showAll();
	cout << endl;
	b.showAll();
	cout << endl;
	c.showAll();
	cout << endl;
}

void FinanseController::showInfo()
{
	KierownikController a;
	PracownikController b;
	ZespolController c;
	cout << "Sumaryczna wartosc pensji wszystkich pracownikow i kierownikow wynosi: " << a.sumaKier() + b.sumaPrac() + c.sumaZesp() << endl;
}
