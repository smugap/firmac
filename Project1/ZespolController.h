#pragma once
#include <iostream>
#include "Kierownik.h"
#include <string>
#include <vector>
#include "File.h"
#include "View.h"
#include "KierownikController.h"
#include "PracownikController.h"
using namespace std;
class ZespolController
{
	friend class File;
	vector<Zespol> zespoly;
public:
	void saveToF();
	void showAll();
	void addZespol();
	void editZespol(string nazwa,char opcja);
	void deleteZespol(string nazwa);
	double sumaZesp();
	ZespolController();
	~ZespolController();
};

