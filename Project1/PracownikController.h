#pragma once
#include <iostream>
#include "Pracownik.h"
#include <string>
#include <vector>
#include "File.h"
#include "View.h"

using namespace std;
class PracownikController
{
	vector<Pracownik> workersVector;
public:
	void addWorker(string imie="brak", string skill="brak");
	void deleteWorker(int id);
	void editWorker(int id,int opcja);
	void showAll();
	void saveToF();
	double sumaPrac();
	PracownikController();
	~PracownikController();
};

