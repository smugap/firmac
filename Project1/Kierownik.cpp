#include "Kierownik.h"


Kierownik::Kierownik() :Osoba()
{
	m_salary *= 3;
	m_stanowisko = "Kierownik";
	m_auto = true;
	m_imie = "Piotr";
}

Kierownik::Kierownik(const Kierownik & rhs) :Osoba(rhs)
{
	m_auto = rhs.m_auto;
}


Kierownik::Kierownik(string imie, bool samochod) :Osoba(imie,"Kierownik")
{

	m_auto = samochod;
	m_salary *= 3;
}

Kierownik::~Kierownik()
{
	
}

void Kierownik::setSalary(double Salary)
{
	m_salary = Salary;
}

void Kierownik::show()
{
	Osoba::show();
	cout << setw(14) << "Auto: " << setw(15) << m_auto << " | " << endl;
}
string Kierownik::wtp()
{
	string pomoc = Osoba::wtp();
	ostringstream a;
	a << m_auto;
	pomoc += a.str();
	
	return pomoc;
}

void Kierownik::setAuto(bool au)
{
	m_auto = au;
}
