#include "KierownikController.h"



void KierownikController::addBoss(string imie, bool samochod)
{
	int pomoc = bossVector.size() + 1;
	bossVector.resize(bossVector.size() + 1, Kierownik(imie, samochod));
	this->saveToF();
}

void KierownikController::deleteBoss(int id)
{
	int pomoc = 0;
	for each (auto var in bossVector)
	{

		if (var.m_id == id)
		{
			bossVector.erase(bossVector.begin() + pomoc);
			break;
		}
		pomoc++;
	}
}

void KierownikController::editBoss(int id, int opcja)
{
	switch (opcja)
	{
	case 1:
	{
		int pomoc = 0;
		for each (auto var in bossVector)
		{

			if (var.m_id == id)
			{
				string im;
				cout << "Podaj imie: ";
				cin >> im;
				bossVector[pomoc].m_imie = im;
				break;
			}
			pomoc++;
		}
		break;
	}
	case 2:
	{
		int pomoc2 = 0;
		for each (auto var in bossVector)
		{

			if (var.m_id == id)
			{
				int im;
				cout << "Podaj place: ";
				cin >> im;
				bossVector[pomoc2].m_salary = im;
				break;
			}
			pomoc2++;
		}
		break;
	}

	case 3:
	{
		int pomoc3 = 0;
		for each (auto var in bossVector)
		{

			if (var.m_id == id)
			{
				string im;
				cout << "Przyznac samochod? [1] - tak, [0] - nie: ";
				cin >> im;
				bossVector[pomoc3].setAuto((im=="1")?true:false);
				break;
			}
			pomoc3++;
		}
		break;
	}

	default:
		break;
	}
}

void KierownikController::showAll()
{
	for each (auto var in bossVector)
	{
		View::Show(var);
	}
}

void KierownikController::saveToF()
{
	File::setName("Kierownik.csv");
	File::writeKier(bossVector);
}

double KierownikController::sumaKier()
{
	double suma = 0;
	for each (auto var in bossVector)
	{
		suma += var.m_salary;
	}
	return suma;
}

KierownikController::KierownikController()
{
	File::setName("Kierownik.csv");
	bossVector = *File::readKier();
}


KierownikController::~KierownikController()
{
	saveToF();
	bossVector.~vector();
	bossVector.clear();
	bossVector.shrink_to_fit();
}
