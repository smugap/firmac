#include "File.h"

string File::nazwa_pliku = "test.csv";

bool File::stob(string & A)
{
	if (A == "1")
	{
		return true;
	}
	else
		return false;
}

void File::setName(string name)
{
	nazwa_pliku = name;
}

void File::writeZespol(Zespol & rhs)
{
	std::fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::out | ios::trunc);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}
	catch (...)
	{
		cout << "Permament error" << endl;
	}
	

	if (plik.good() == true)
	{

		try
		{
			plik << rhs.m_nazwa << "," << rhs.m_kierownik.wtp() << ",";

			for (int i = 0; i < 10; i++)
			{
				plik << rhs.tab[i].wtp() << ",";
			}
			plik << endl;
		}
		catch (const std::exception& A)
		{
			cout << A.what() << endl;
		}

		plik.close();
	}
	
}

Zespol * File::readZespol()
{
	fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::in);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
		return nullptr;
	}

	Zespol* zespol;

	if (plik.good() == true)
	{
		string pomoc;

		zespol = new Zespol();

		stringstream A;
		A << plik.rdbuf();

		getline(A, pomoc, ',');
		zespol->setName(pomoc);
		pomoc.clear();
		
		
			getline(A, pomoc, ',');
			zespol->m_kierownik.setID(stoi(pomoc));
			pomoc.clear();
		
			getline(A, pomoc, ',');
			zespol->m_kierownik.setImie(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			zespol->m_kierownik.setStanowisko(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			zespol->m_kierownik.setSalary(stod(pomoc));
			pomoc.clear();

			getline(A, pomoc, ',');
			zespol->m_kierownik.setAuto(stob(pomoc));
			pomoc.clear();
		
			for (int  i = 0; i < 10; i++)
			{
				getline(A, pomoc, ',');
				zespol->tab[i].setID(stoi(pomoc));
				pomoc.clear();

				getline(A, pomoc, ',');
				zespol->tab[i].setImie(pomoc);
				pomoc.clear();

				getline(A, pomoc, ',');
				zespol->tab[i].setStanowisko(pomoc);
				pomoc.clear();

				getline(A, pomoc, ',');
				zespol->tab[i].setSalary(stod(pomoc));
				pomoc.clear();

				getline(A, pomoc, ',');
				zespol->tab[i].setSkill(pomoc);
				pomoc.clear();
			}

		plik.close();
		return zespol;
	}

	return nullptr;
}

void File::writePracownik(Pracownik * A)
{
	std::fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::out | ios::trunc);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}
	catch (...)
	{
		cout << "Permament error" << endl;
	}


	if (plik.good() == true)
	{

		try
		{
			plik << A->wtp() << ",";
			plik << endl;
		}
		catch (const std::exception& A)
		{
			cout << A.what() << endl;
		}

		plik.close();
	}
}

Pracownik * File::readPracownik()
{
	fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::in);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
		return nullptr;
	}

	
	Pracownik* worker;

	if (plik.good() == true)
	{
		string pomoc;

		worker = new Pracownik();

		stringstream A;
		A << plik.rdbuf();

		getline(A, pomoc, ',');
		worker->setID(stoi(pomoc));
		pomoc.clear();

		getline(A, pomoc, ',');
		worker->setImie(pomoc);
		pomoc.clear();

		getline(A, pomoc, ',');
		worker->setStanowisko(pomoc);
		pomoc.clear();

		getline(A, pomoc, ',');
		worker->setSalary(stod(pomoc));
		pomoc.clear();

		getline(A, pomoc, ',');
		worker->setSkill(pomoc);
		pomoc.clear();

		plik.close();
		return worker;
	}

	return nullptr;
}

void File::writeKierownik(Kierownik * A)
{

	std::fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::out | ios::trunc);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}
	catch (...)
	{
		cout << "Permament error" << endl;
	}


	if (plik.good() == true)
	{

		try
		{
			plik << A->wtp() << ",";
			plik << endl;
		}
		catch (const std::exception& A)
		{
			cout << A.what() << endl;
		}

		plik.close();
	}
}

Kierownik * File::readKierownik()
{
	fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::in);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
		return nullptr;
	}


	Kierownik* kierownik;

	if (plik.good() == true)
	{
		string pomoc;

		kierownik = new Kierownik();

		stringstream A;
		A << plik.rdbuf();

		getline(A, pomoc, ',');
		kierownik->setID(stoi(pomoc));
		pomoc.clear();

		getline(A, pomoc, ',');
		kierownik->setImie(pomoc);
		pomoc.clear();

		getline(A, pomoc, ',');
		kierownik->setStanowisko(pomoc);
		pomoc.clear();

		getline(A, pomoc, ',');
		kierownik->setSalary(stod(pomoc));
		pomoc.clear();

		getline(A, pomoc, ',');
		kierownik->setAuto(stob(pomoc));
		pomoc.clear();

		plik.close();
		return kierownik;
	}

	return nullptr;
}

vector<Zespol>* File::readZesp()
{
	fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::in);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
		return nullptr;
	}

	vector<Zespol>* zespoly;

	if (plik.good() == true)
	{
		string pomoc;
		zespoly = new vector<Zespol>();

		stringstream A;
		A << plik.rdbuf();

		getline(A, pomoc, ',');
		
		int ileZesp = stoi(pomoc);
		pomoc.clear();

		for (int i = 0; i < ileZesp; i++)
		{
			Zespol * nowy = new Zespol();

			getline(A, pomoc, ',');
			nowy->setName(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			nowy->m_kierownik.setID(stoi(pomoc));
			pomoc.clear();

			getline(A, pomoc, ',');
			nowy->m_kierownik.setImie(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			nowy->m_kierownik.setStanowisko(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			nowy->m_kierownik.setSalary(stod(pomoc));
			pomoc.clear();

			getline(A, pomoc, ',');
			nowy->m_kierownik.setAuto(stob(pomoc));
			pomoc.clear();


			for (int i = 0; i < 10; i++)
			{
				getline(A, pomoc, ',');
				nowy->tab[i].setID(stoi(pomoc));
				pomoc.clear();

				getline(A, pomoc, ',');
				nowy->tab[i].setImie(pomoc);
				pomoc.clear();

				getline(A, pomoc, ',');
				nowy->tab[i].setStanowisko(pomoc);
				pomoc.clear();

				getline(A, pomoc, ',');
				nowy->tab[i].setSalary(stod(pomoc));
				pomoc.clear();

				getline(A, pomoc, ',');
				nowy->tab[i].setSkill(pomoc);
				pomoc.clear();
			}


			zespoly->push_back(*nowy);
		}

		
		plik.close();
		return zespoly;
	}

	return nullptr;
}

void File::writeZesp(vector<Zespol>& rhs)
{
	std::fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::out | ios::trunc);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}
	catch (...)
	{
		cout << "Permament error" << endl;
	}
	if (plik.good() == true)
	{
		try
		{
			plik << rhs.size() << ",";
			for (int i = 0; i < rhs.size(); i++)
			{
				plik << rhs[i].m_nazwa << ",";
				plik << rhs[i].m_kierownik.wtp() << ",";

				for (int j = 0; j < 10; j++)
				{
					plik << rhs[i].tab[j].wtp() << ",";
				}
			}
			plik << endl;
		}
		catch (const std::exception& A)
		{
			cout << A.what() << endl;
		}

		plik.close();
	}
}

vector<Pracownik>* File::readPr()
{
	fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::in);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}


	vector<Pracownik>* workers;

	if (plik.good() == true)
	{
		string pomoc;
		stringstream A;
		A << plik.rdbuf();
		getline(A, pomoc, ',');
		int rozmiarWorkers = stoi(pomoc);
		pomoc.clear();

		workers = new vector<Pracownik>();

		
		for (int i = 0; i < rozmiarWorkers; i++)
		{
			Pracownik* p = new Pracownik();
			getline(A, pomoc, ',');
			p->setID(stoi(pomoc));
			pomoc.clear();
			

			getline(A, pomoc, ',');
			p->setImie(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			p->setStanowisko(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			p->setSalary(stod(pomoc));
			pomoc.clear();

			getline(A, pomoc, ',');
			p->setSkill(pomoc);
			pomoc.clear();
			workers->push_back(*p);
		}
		

		plik.close();
		return workers;
	}

}

void File::writePr(vector<Pracownik>& rhs)
{
	std::fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::out | ios::trunc);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}
	catch (...)
	{
		cout << "Permament error" << endl;
	}


	if (plik.good() == true)
	{

		try
		{
			plik << rhs.size() << "," << endl;

			for (int i = 0; i < rhs.size(); i++)
			{
				plik << rhs[i].wtp()<< ",";
				plik << endl;
			}
			
		}
		catch (const std::exception& A)
		{
			cout << A.what() << endl;
		}

		plik.close();
	}

}

vector<Kierownik>* File::readKier()
{
	fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::in);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}


	vector<Kierownik>* kierownicy;

	if (plik.good() == true)
	{
		string pomoc;
		stringstream A;
		A << plik.rdbuf();
		getline(A, pomoc, ',');
		int rozmiarKierownicy = stoi(pomoc);
		pomoc.clear();

		kierownicy = new vector<Kierownik>();


		for (int i = 0; i < rozmiarKierownicy; i++)
		{
			Kierownik* p = new Kierownik();
			getline(A, pomoc, ',');
			p->setID(stoi(pomoc));
			pomoc.clear();


			getline(A, pomoc, ',');
			p->setImie(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			p->setStanowisko(pomoc);
			pomoc.clear();

			getline(A, pomoc, ',');
			p->setSalary(stod(pomoc));
			pomoc.clear();

			getline(A, pomoc, ',');
			p->setAuto(stob(pomoc));
			pomoc.clear();
			kierownicy->push_back(*p);
		}


		plik.close();
		return kierownicy;
	}
}

void File::writeKier(vector<Kierownik>& rhs)
{
	std::fstream plik;

	try
	{
		plik.open(nazwa_pliku, ios::out | ios::trunc);
	}
	catch (const std::exception& A)
	{
		cout << A.what() << endl;
	}
	catch (...)
	{
		cout << "Permament error" << endl;
	}


	if (plik.good() == true)
	{

		try
		{
			plik << rhs.size() << "," << endl;

			for (int i = 0; i < rhs.size(); i++)
			{
				plik << rhs[i].wtp() << ",";
				plik << endl;
			}

		}
		catch (const std::exception& A)
		{
			cout << A.what() << endl;
		}

		plik.close();
	}

}

