#include "ZespolController.h"


void ZespolController::saveToF()
{
	File::setName("Zespol.csv");
	File::writeZesp(zespoly);
}

void ZespolController::showAll()
{
	for each (auto var in zespoly)
	{
		View::Print_Zespol(var);
	}
	cout << endl;
}

void ZespolController::addZespol()
{
	int pomoc = zespoly.size() + 1;
	cout << "Podaj nazwe zespolu: ";
	string nazwa;
	cin >> nazwa;
	zespoly.push_back(Zespol(nazwa));
}

void ZespolController::editZespol(string nazwa, char opcja)
{

	for each (auto var in zespoly)
	{

		if (var.m_nazwa == nazwa)
		{
			switch (opcja)
			{
			case 'n':
			{
				cout << "Podaj nowa nazwe zespolu" << endl;
				string mnazwa;
				cin >> mnazwa;
				var.setName(mnazwa);
				break;
			}	
			case 'k':
			{
				cout << "Podaj id kierownika: ";
				int nr;
				cin >> nr;
				KierownikController kk;
				
				var.m_kierownik = kk.bossVector[nr];
				break;
			}					
			default:
				break;
			}
			//break;
		}
	}
	saveToF();
}

void ZespolController::deleteZespol(string nazwa)
{
	int pomoc = 0;
	for each (auto var in zespoly)
	{

		if (var.m_nazwa == nazwa)
		{
			zespoly.erase(zespoly.begin() + pomoc);
			break;
		}
		pomoc++;
	}
}

double ZespolController::sumaZesp()
{
	double suma = 0;
	for each (auto var in zespoly)
	{
		suma += var.m_kierownik.m_salary;
		for each (auto var in var.tab)
		{
			suma += var.m_salary;
		}
	}
	return suma;
}

ZespolController::ZespolController()
{
	File::setName("Zespol.csv");
	zespoly = *File::readZesp();
}


ZespolController::~ZespolController()
{
	saveToF();
	zespoly.~vector();
	zespoly.clear();
	zespoly.shrink_to_fit();
}
