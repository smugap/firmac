﻿#include "Display.h"

void Display::menuZespol()
{
	ZespolController pra;
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("Zespol", "Zarzadzanie zespolem");
		cout << "\n\t1. Pokaz wszystkie zespoly" << endl;
		cout << "\t2. Dodaj zespol" << endl;
		cout << "\t3. Edytuj zespol" << endl;
		cout << "\t4. Usun zespol\n" << endl;
		cout << "\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			menuZespolShowAll(pra);
			break;
		case '2':
			menuAddZespol(pra);
			break;
		case '3':
			menuZespolEdit(pra);
			break;
		case '4':
			menuDeleteZespol(pra);
			break;
		case '9':
			break;
		default:
			break;
		}
	} while (opcja != '9');
}

void Display::menuZespolShowAll(ZespolController &pra)
{
	wyczyscEkran();
	rysuj("Zespol", "");
	cout << "\n\n";
	pra.showAll();
	cout << endl;
	system("pause");
}

void Display::menuAddZespol(ZespolController &pra)
{
	wyczyscEkran();
	rysuj("Zespol", "Dodaj nowy");
	pra.addZespol();
	pra.saveToF();
	system("pause");
}

void Display::menuDeleteZespol(ZespolController & pra)
{
	wyczyscEkran();
	rysuj("Zespol", "Usun zespol");
	cout << "\n\n Podaj nazwe zespolu do usuniecia: ";
	string pomoc;
	cin >> pomoc;
	pra.deleteZespol(pomoc);
	pra.saveToF();
	cout << endl;
	system("pause");
}

void Display::menuZespolEdit(ZespolController &pra)
{
	wyczyscEkran();
	rysuj("Zespol", "Edytuj zespol");
	cout << "\n\n";
	cout << "Podaj nazwe zespolu do edycji: ";
	string naz;
	cin >> naz;
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("Zespol", "Edytuj zespol");
		cout << "\n\n";
		cout << "\t1. Zmien nazwe zespolu" << endl;
		cout << "\t2. Zmien kierownika zespolu" << endl;
		cout << endl;
		cout << "\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			cin.ignore();
			pra.editZespol(naz, 'n');
			pra.saveToF();
			break;
		case '2':
			pra.editZespol(naz, 'k');
			pra.saveToF();
			break;
		case '9':
			pra.saveToF();
			break;
		default:
			break;
		}
	} while (opcja!='9');
}

void Display::menuPracownik()
{
	PracownikController pra;
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("Pracownik", "Zarzadzanie pracownikami");
		cout << "\n\t1. Dodaj pracownika" << endl;
		cout << "\t2. Edytuj pracownika" << endl;
		cout << "\t3. Usun pracownika" << endl;
		cout << "\t4. Wyswietl wszystkich pracownikow" << endl;
		cout << "\n\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			menuPracownikAdd(pra);
			pra.saveToF();
			break;
		case '2':
			menuPracownikEdit(pra);
			break;
		case '3':
			menuPracownikDelete(pra);
			break;
		case '4':
			menuPracownikShow(pra);
			break;
		case '9':
			break;
		default:
			break;
		}
	} while (opcja!='9');
}

void Display::menuKierownik()
{
	KierownikController pra;
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("Kierownik", "Zarzadzanie kierownikami");
		cout << "\n\t1. Dodaj kierownika" << endl;
		cout << "\t2. Edytuj kierownika" << endl;
		cout << "\t3. Usun kierownika" << endl;
		cout << "\t4. Wyswietl wszystkich kierownikow" << endl;
		cout << "\n\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			menuKierownikAdd(pra);
			pra.saveToF();
			break;
		case '2':
			menuKierownikEdit(pra);
			break;
		case '3':
			menuKierownikDelete(pra);
			break;
		case '4':
			menuKierownikShow(pra);
			break;
		case '9':
			break;
		default:
			break;
		}
	} while (opcja!='9');
}

void Display::menuFinanse()
{
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("", "Finanse");
		cout << "\n\t1. Wyswietl wszystkich zatrudnionych" << endl;
		cout << "\t2. Wydatki" << endl;
		cout << "\n\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			menuFinanseAll();
			break;
		case '2':
			menuFinanseWydatki();
			break;
		case '9':
			break;
		default:
			break;
		}
	} while (opcja!='9');
}

void Display::menuAbout()
{
	wyczyscEkran();
	rysuj("About", "Zarzadzanie personelem wersja 1.0","Autor: Piotr Smuga");
	cout << "\n\t";
	system("pause");
}

void Display::menuExit(char & opcja)
{
	char wybor;
	wyczyscEkran();
	rysuj("Wyjscie", "");
	cout << endl << "Na pewno chcesz wyjsc? (T/N) " << endl;
	wybor = _getch();
	if (tolower(wybor) == 't')
	{
		opcja = '0';
		this->~Display();
	}
	else
		opcja = 'e';
}

void Display::menuKierownikAdd(KierownikController &pra)
{
	wyczyscEkran();
	rysuj("Kierownik", "Dodaj nowego kierownika");
	pra.addBoss();
	system("pause");
}

void Display::menuKierownikShow(KierownikController &pra)
{
	wyczyscEkran();
	rysuj("Kierownik", "Lista wszystkich kierownikow");
	cout << "\n\n";
	pra.showAll();
	cout << "\n\tPress any key to continue . . ." << endl;
}

void Display::menuKierownikEdit(KierownikController &pra)
{
	wyczyscEkran();
	rysuj("Kierownik", "Edytuj kierownika");
	cout << "\n\n";
	cout << "Podaj id kierownika: ";
	int pomocId;
	cin >> pomocId;
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("Kierownik", "Edytuj kierownika");
		cout << "\n\n";
		cout << "\t1. Zmien imie" << endl;
		cout << "\t2. Zmien pensje" << endl;
		cout << "\t3. Zmien przynaleznosc samochodu" << endl;
		cout << endl;
		cout << "\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			pra.editBoss(pomocId, 1);
			pra.saveToF();
			break;
		case '2':
			pra.editBoss(pomocId, 2);
			pra.saveToF();
			break;
		case '3':
			pra.editBoss(pomocId, 3);
			pra.saveToF();
			break;
		case '9':
			pra.saveToF();
			break;
		default:
			break;
		}
	} while (opcja!='9');
}

void Display::menuKierownikDelete(KierownikController &pra)
{
	wyczyscEkran();
	rysuj("Kierownik", "Usun kierownika");
	cout << "\n\n Podaj id kierownika do usuniecia: ";
	int pomoc;
	cin >> pomoc;
	pra.deleteBoss(pomoc);
	pra.saveToF();
	cout << endl;
	system("pause");
}

void Display::menuPracownikAdd(PracownikController &pra)
{
	wyczyscEkran();
	rysuj("Pracownik", "Dodaj nowego pracownika");
	pra.addWorker();
	system("pause");
}

void Display::menuPracownikShow(PracownikController &pra)
{
	wyczyscEkran();
	rysuj("Pracownik", "Lista wszystkich pracownikow");
	cout << "\n\n";
	pra.showAll();
	cout << endl;
	system("pause");
}

void Display::menuPracownikEdit(PracownikController &pra)
{
	wyczyscEkran();
	rysuj("Pracownik", "Edytuj pracownika");
	cout << "\n\n";
	cout << "Podaj id pracownika: ";
	int pomocId;
	cin >> pomocId;
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("Pracownik", "Edytuj pracownika");
		cout << "\n\n";
		cout << "\t1. Zmien imie" << endl;
		cout << "\t2. Zmien pensje" << endl;
		cout << "\t3. Zmien umiejetnosc" << endl;
		cout << endl;
		cout << "\t9. Powrot" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			pra.editWorker(pomocId, 1);
			pra.saveToF();
			break;
		case '2':
			pra.editWorker(pomocId, 2);
			pra.saveToF();
			break;
		case '3':
			pra.editWorker(pomocId, 3);
			pra.saveToF();
			break;
		case '9':
			pra.saveToF();
			break;
		default:
			break;
		}
	} while (opcja!='9');
}

void Display::menuPracownikDelete(PracownikController &pra)
{
	wyczyscEkran();
	rysuj("Pracownik", "Usun pracownika");
	cout << "\n\n Podaj id pracownika do usuniecia: ";
	int pomoc;
	cin >> pomoc;
	pra.deleteWorker(pomoc);
	pra.saveToF();
	cout << endl;
	system("pause");
}

void Display::menuFinanseWydatki()
{
	wyczyscEkran();
	rysuj("Finanse", "Wydatki na pracownikow");
	cout << "\n\n";
	FinanseController::showInfo();
	cout << endl;
	system("pause");
}

void Display::menuFinanseAll()
{
	wyczyscEkran();
	rysuj("Finanse", "Wszyscy zatrudnieni");
	cout << "\n\n";
	FinanseController::showAll();
	cout << endl;
	system("pause");
}

void Display::rysuj(string komunikat,string tresc,string opcja)
{
	setlocale(LC_ALL, "pl-PL");

	auto pomoc = komunikat;
	int ile2 = tresc.size() - komunikat.size();
	if (ile2 > 0)
		pomoc += string(ile2, ' ');
	if (tresc.size() < pomoc.size())
		tresc += string(pomoc.size() - tresc.size(), ' ');
	int ile = tresc.size() + 6;
	if (opcja.size() < tresc.size())
		opcja += string(pomoc.size()-opcja.size(), ' ');
	cout << string(ile, '#') << endl;
	cout << "## " << pomoc << " ##" << endl;
	cout << "## " << tresc << " ##" << endl;
	cout << "## " << opcja << " ##" << endl;
	cout << string(ile, '#') << endl;

}

void Display::wyczyscEkran()
{
	system("cls");
}

void Display::rysujMenu()
{
	setlocale(LC_ALL, "pl-PL");
	char opcja;
	do
	{
		wyczyscEkran();
		rysuj("", "Program Zarzadzania firma");
		cout << "\nMenu glowne" << endl;
		cout << "\t1. Zespol" << endl;
		cout << "\t2. Pracownik" << endl;
		cout << "\t3. Kierownik" << endl;
		cout << "\t4. Finanse" << endl;
		cout << "\n\t9. O programie ..." << endl;
		cout << "\t0. Wyjscie" << endl;
		opcja = _getch();
		switch (opcja)
		{
		case '1':
			menuZespol();
			break;
		case '2':
			menuPracownik();
			break;
		case '3':
			menuKierownik();
			break;
		case '4':
			menuFinanse();
			break;
		case '9':
			menuAbout();
			break;
		case '0':
			menuExit(opcja);
			break;
		default:
			break;
		}
	} while (opcja!='0');
}

Display::Display()
{
}
Display::~Display()
{
}
