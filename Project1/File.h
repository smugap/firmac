#pragma once
#include <sstream>
#include <iostream>
#include <string>
#include <fstream>
#include "Zespol.h"		
#include "Osoba.h"

using namespace std;
class File
{
	static inline bool stob(string& A);
	static string nazwa_pliku;
public:
	static void setName(string name);
	static void writeZespol(Zespol& rhs);
	static Zespol* readZespol();
	static void writePracownik(Pracownik* A);
	static Pracownik* readPracownik();
	static void writeKierownik(Kierownik* A);
	static Kierownik* readKierownik();

	static vector<Zespol>* readZesp();
	static void writeZesp(vector<Zespol>& rhs);

	static vector<Pracownik>* readPr();
	static void writePr(vector<Pracownik>& rhs);

	static vector<Kierownik>* readKier();
	static void writeKier(vector<Kierownik>& rhs);
};

