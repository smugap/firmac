#include "PracownikController.h"



void PracownikController::addWorker(string imie, string skill)
{
	int pomoc = workersVector.size()+1;
	workersVector.resize(workersVector.size() + 1, Pracownik(imie, skill));
	//workersVector.insert(workersVector.end(), Pracownik(imie, skill));
	//workersVector.push_back(Pracownik(imie, skill));
}

void PracownikController::deleteWorker(int id)
{
	int pomoc = 0;
	for each (auto var in workersVector)
	{
		
		if (var.m_id == id)
		{
			workersVector.erase(workersVector.begin()+pomoc) ;
			break;
		}
		pomoc++;
	}
}

void PracownikController::editWorker(int id, int opcja)
{
	switch (opcja)
	{
	case 1:
	{
		int pomoc = 0;
		for each (auto var in workersVector)
		{

			if (var.m_id == id)
			{
				string im;
				cout << "Podaj imie: ";
				cin >> im;
				workersVector[pomoc].m_imie = im;
				break;
			}
			pomoc++;
		}
		break;
	}		
	case 2:
	{
		int pomoc2 = 0;
		for each (auto var in workersVector)
		{

			if (var.m_id == id)
			{
				int im;
				cout << "Podaj place: ";
				cin >> im;
				workersVector[pomoc2].m_salary = im;
				break;
			}
			pomoc2++;
		}
		break;
	}
		
	case 3:
	{
		int pomoc3 = 0;
		for each (auto var in workersVector)
		{

			if (var.m_id == id)
			{
				string im;
				cout << "Podaj umiejetnosc: ";
				cin >> im;
				workersVector[pomoc3].setSkill(im);
				break;
			}
			pomoc3++;
		}
		break;
	}
		
	default:
		break;
	}
}

void PracownikController::showAll()
{

	for each (auto var in workersVector)
	{
		View::Show(var);
	}
}

void PracownikController::saveToF()
{
	File::setName("Pracownik.csv");
	File::writePr(workersVector);
}

double PracownikController::sumaPrac()
{
	double suma = 0;
	for each (auto var in workersVector)
	{
		suma += var.m_salary;
	}
	return suma;
}

PracownikController::PracownikController()
{
	//wczytane z pliku
	File::setName("Pracownik.csv");
	workersVector = *File::readPr();

}



PracownikController::~PracownikController()
{
	saveToF();
	workersVector.~vector();
	workersVector.clear();
	workersVector.shrink_to_fit();
}
