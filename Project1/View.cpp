#include "View.h"




void View::Print(Osoba & rhs)
{
	cout << setprecision(3);
	
	cout << " | " << rhs.m_id << " | " << setw(10) << rhs.m_imie <<" | " << setw(10)<< rhs.m_stanowisko 
		<< " | " << setprecision(8) << setw(8)  << rhs.m_salary << " | " << endl;
}

void View::Show(Osoba & rhs)
{
	rhs.show();
}

void View::Print_Zespol(Zespol & rhs)
{
	cout << rhs.m_nazwa << endl;
	Show(rhs.m_kierownik);
	for each (Pracownik var in rhs.tab)
	{
		Show(var);
	}
}
