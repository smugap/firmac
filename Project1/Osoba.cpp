#include "Osoba.h"


int Osoba::m_iid = 0;

Osoba::Osoba()
{
	m_id = m_iid;
	m_iid++;
	m_stanowisko = "";
	m_salary = 10000;
	m_imie = "brak";
}

Osoba::Osoba(const Osoba & rhs)
{
	m_id = rhs.m_id;
	m_iid++;
	m_stanowisko = rhs.m_stanowisko;
	m_salary = rhs.m_salary;
	m_imie = rhs.m_imie;
}

Osoba::Osoba(string imie, string stanowisko)
{
	m_id = m_iid;
	m_iid++;
	m_imie = imie;
	m_stanowisko = stanowisko;
	m_salary = 10000;
}

Osoba::~Osoba()
{
	m_iid--;
	
}

void Osoba::setID(int id)
{
	m_id = id;
}

void Osoba::setImie(string im)
{
	m_imie = im;
}

void Osoba::setStanowisko(string stanowisko)
{
	m_stanowisko = stanowisko;
}

void Osoba::show()
{
	cout << " | "  << setw(3)<< m_id << " | " << setw(10) << m_imie << " | " << setw(10) << m_stanowisko
		<< " | " << setprecision(6) << m_salary << " | ";
}

string Osoba::wtp()
{
	ostringstream a,b;
	a << m_salary;
	b << m_id;
	string pomoc;
	pomoc = b.str() + "," + m_imie + "," + m_stanowisko + "," + a.str() + ",";
	return pomoc;
}
