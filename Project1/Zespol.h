﻿#pragma once
#include <iostream>
#include <string>
#include "Kierownik.h"
#include "Pracownik.h"
#include <vector>

using namespace std;
class Zespol
{
	friend class File;
	friend class View;
	friend class ZespolController;
	string m_nazwa;
	Kierownik m_kierownik;
	vector<Pracownik> tab;
public:
	Zespol(string nazwa="Zespol");
	~Zespol();
	void setName(string name);
};

