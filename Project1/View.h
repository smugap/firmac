#pragma once
#include <iostream>
#include "Osoba.h"
#include <iomanip>
#include "Zespol.h"
using namespace std;
class View
{
public:
	static void Print(Osoba& rhs);
	static void Show(Osoba& rhs);
	static void Print_Zespol(Zespol& rhs);
};

