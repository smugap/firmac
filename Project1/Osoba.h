#pragma once
#include <iostream>
#include <string>
#include <iomanip>
#include <sstream>
#include <fstream>

using namespace std;
class Osoba
{
	friend class View;
	friend class Zespol;
	friend class PracownikController;
	friend class KierownikController;
	friend class ZespolController;
protected:
	int m_id;
	static int m_iid;
	string m_imie;
	string m_stanowisko;
	double m_salary;
public:
	Osoba();
	Osoba(const Osoba& rhs);
	Osoba(string imie, string stanowisko);
	virtual ~Osoba() ;
	virtual void setSalary(double Salary) = 0;

	void setID(int id);
	void setImie(string im);
	void setStanowisko(string stanowisko);

	virtual void show() ;
	virtual string wtp();
};

