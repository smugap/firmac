#pragma once
#include "Osoba.h"

class Kierownik :
	public Osoba
{
	bool m_auto;
	friend class Zespol;
public:
	Kierownik();
	Kierownik(const Kierownik& rhs);
	Kierownik(string imie, bool samochod);
	~Kierownik();
	void setSalary(double Salary) override;
	void show() override;
	string wtp();
	void setAuto(bool au);
	
};

