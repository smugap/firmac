#pragma once
#include <iostream>
#include "Kierownik.h"
#include <string>
#include <vector>
#include "File.h"
#include "View.h"

using namespace std;
class KierownikController
{
	friend class ZespolController;
	vector<Kierownik> bossVector;
public:
	void addBoss(string imie = "brak", bool samochod = true);
	void deleteBoss(int id);
	void editBoss(int id, int opcja);
	void showAll();
	void saveToF();
	double sumaKier();
	KierownikController();
	~KierownikController();
};

